<?php

/*
 * Class used to represent a Viadutoo job stored by Resque in a Redis queue.
 * Will contain properties such as the Caliper event and configuration values
 * and methods to process the event, either sending it to an endpoint or
 * storing it in a DB.
 */

class ViadutooJob {

}
